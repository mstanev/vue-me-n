import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/**
 * Router collection
 *
 * - Expected data in the collection is very simple
 * @example
 * {
 *  abstract: [boolean],
 *  path: string,
 *  name: string,
 *  component: (function(): (Promise<*>|*)),
 *  children: [{path: string, name: string, component: (function(): (Promise<*>|*))}[]]
 * }
 * @type {*[]}
 */
export const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('../views/Dashboard'),
  },
  {
    abstract: true,
    path: '/clients',
    name: 'Clients',
    component: () => import('../views/Clients'),
    children: [
      {
        path: 'regular-grid',
        name: 'Clients Regular Grid',
        component: () => import('../modules/clients'),
      },
      // TODO: do that :)
      {
        path: 'editable-grid',
        name: 'Clients Editable Grid',
        component: () => import('../modules/clients'),
      },
    ],
  },
];

/**
 * Instance of Vue router
 * @type {VueRouter}
 */
const route = new Router({
  routes,
});

export default route;
