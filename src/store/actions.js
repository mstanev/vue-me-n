import route from '../router';

const initCurrentRoute = (context) => {
  route.afterEach(() => context.commit(
    'CHANGE_CURRENT_ROUTE',
    route.currentRoute,
  ));
};

const attachRouteEventsToUI = (context) => {
  const { classList } = document.body;
  const commitIt = () => context.commit('CHANGE_UI_STATE', { className: document.body.className });

  // It will add loader and hide content
  route.beforeEach((to, from, next) => {
    classList.add('loading');
    next();
    commitIt();
  });

  // It will Remove loader and show content
  // Add delay if it's necessary
  route.afterEach(() => {
    classList.remove('loading');
    commitIt();
  });
};

export default {
  attachRouteEventsToUI,
  initCurrentRoute,
};
