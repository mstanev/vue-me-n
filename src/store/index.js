import Vue from 'vue';
import Vuex from 'vuex';
import { routes } from '../router';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({});

const state = {
  routes,
  uiState: {}, // TBD
};


store.registerModule('$_routes', {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
});

/**
 * Bootstrap App
 */
// Description
store.dispatch('$_routes/initCurrentRoute');
// Description
store.dispatch('$_routes/attachRouteEventsToUI');

export default store;
