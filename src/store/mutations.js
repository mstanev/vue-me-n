const CHANGE_CURRENT_ROUTE = (state, data) => {
  state.currentRoute = data;
};

const CHANGE_UI_STATE = (state, data) => {
  state.uiState = data;
};

export default {
  CHANGE_CURRENT_ROUTE,
  CHANGE_UI_STATE,
};
