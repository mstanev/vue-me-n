const getRoutes = state => state.routes;
const currentRoute = state => state.currentRoute;
const getUiState = status => status.uiState;

export default {
  getRoutes,
  currentRoute,
  getUiState,
};
