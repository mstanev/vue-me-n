const layoutType = state => state.layoutType;
const layoutTypes = state => state.layoutTypes;

export default {
  layoutType, layoutTypes,
};
