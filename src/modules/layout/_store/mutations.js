const CHANGE_LAYOUT_TYPE = (state, layoutType) => {
  state.layoutType = layoutType;
};

export default {
  CHANGE_LAYOUT_TYPE,
};
