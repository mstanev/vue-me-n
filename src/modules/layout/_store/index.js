import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const layoutTypes = ['bootstrap', 'pure'];
const defaultLayout = layoutTypes[0];

const state = {
  layoutTypes,
  defaultLayout,
  layoutType: defaultLayout,
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
