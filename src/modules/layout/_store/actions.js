const changeLayoutType = (context, layoutType) => {
  if (context.state.layoutTypes.indexOf(layoutType) !== -1) {
    context.commit('CHANGE_LAYOUT_TYPE', layoutType);
  } else {
    throw new Error(`Assigned layout type "${layoutType}" dose't exist`);
  }
};

export default {
  changeLayoutType,
};
