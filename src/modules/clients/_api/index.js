import { API_BASE_URL, POST, PUT, GET } from '../../../api/constants';

/**
 * @returns {Promise<Response>}
 */
const fetchClients = () =>
  fetch(`${API_BASE_URL}/api/users`, GET())
    .then(result => result.json());

/**
 * @returns {Promise<Response>}
 */
const fetchSchema = () =>
  fetch(`${API_BASE_URL}/api/users/schema`, GET())
    .then(result => result.json());

/**
 * @example https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
 * @param data
 * @returns {Promise<Response>}
 */
const createClient = data =>
  fetch(`${API_BASE_URL}/api/users`, POST(data))
    .then(result => result.json());

const updateClient = data =>
  /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
  fetch(`${API_BASE_URL}/api/users/${data._id}`, PUT(data))
    .then(result => result.json());

export default {
  fetchClients,
  fetchSchema,
  createClient,
  updateClient,
};
