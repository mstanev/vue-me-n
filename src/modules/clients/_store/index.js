import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import { STORE_KEY } from '../constants';

// initial state !!!
const state = {
  collection: [],
  schema: {},
  edit: {},
  create: false,
  pagination: {
    range: {
      from: false,
      to: false,
    },
    itemsPerPage: 'all', // 'all' or an number from {itemsPerPageOptions} collection
    currentPage: 1,
    allPages: this.collection ? this.collection.length : 0,
    itemsPerPageOptions: [3, 10, 20, 50, 100],
    actions: {
      changePage: `${STORE_KEY}/changePaginationPage`,
      changeItemsPerPage: `${STORE_KEY}/changeItemsPerPage`,
    },
    keepStateAfterDestroy: false, // pagination reset ...
  },
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
