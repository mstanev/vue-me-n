function setRange(state) {
  const { pagination } = state;

  if (pagination.itemsPerPage !== 'all') {
    state.pagination.range = {
      from: (pagination.currentPage - 1) * pagination.itemsPerPage,
      to: pagination.currentPage * pagination.itemsPerPage,
    };
  } else {
    state.pagination.range = {
      from: false,
      to: false,
    };
  }
}

const DATA_LOADED = (state, data) => {
  state.collection = data;
  state.pagination.allPages = state.collection.length;
};

const DATA_LOADED_ERROR = (state, error) => {
  state.products = error;
};

const DATA_UPDATED = (state, data) => {
  /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
  state.collection = state.collection.map(item => (data._id === item._id ? data : item));
};

const DATA_UPDATED_ERROR = (state, error) => {
  state.edit = { ...state.edit, error };
};

const DATA_CREATED = (state, data) => {
  state.collection = [data, ...state.collection];
};

const DATA_SCHEMA = (state, data) => {
  state.schema = data;
};

const DATA_EDIT_STATE = (state, data) => {
  state.edit = data;
};

const DATA_CREATE_STATE = (state, data) => {
  state.create = data;
};

const PAGINATION_CHANGE = (state, itemNumber) => {
  state.pagination.currentPage = itemNumber;
  setRange(state);
};

const ITEMS_PER_PAGE = (state, itemsPerPage) => {
  state.pagination.itemsPerPage = itemsPerPage;
  state.pagination.currentPage = 1;
  setRange(state);
};

// TODO: to be implanted and removed...
/* eslint no-unused-vars: "off"  */
const API_ERROR = (state, data) => {};

const asd = '';

export default {
  DATA_LOADED,
  DATA_LOADED_ERROR,
  DATA_UPDATED,
  DATA_UPDATED_ERROR,
  DATA_CREATED,
  DATA_SCHEMA,
  DATA_EDIT_STATE,
  DATA_CREATE_STATE,
  PAGINATION_CHANGE,
  ITEMS_PER_PAGE,
  API_ERROR,
};
