const collection = state => state.collection;
const filteredCollection = state => state.filteredCollection;
/**
 * Component Schema
 * @param state
 * @example
 * {
 *   username: {
 *     type: String,
 *     required: true,
 *     formType: formTypes.InputText,
 *   },
 *   mobileNumber: {
 *     type: String,
 *     minlength: [6, 'Too short'],
 *     maxlength: [12, 'Too long'],
 *     formType: formTypes.InputNumber,
 *   },
 *   OtherNewProp: {
 *     type: Boolean,
 *     default: false,
 *     formType: formTypes.InputCheckbox,
 *   },
 * }
 * @returns {{}|state.schema}
 */
const schema = state => state.schema;
const create = state => state.create;
const edit = state => state.edit;

const pagination = state => state.pagination;

export default {
  collection,
  filteredCollection,
  schema,
  create,
  edit,
  pagination,
};
