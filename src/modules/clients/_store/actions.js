import api from '../_api';
import { STORE_KEY } from '../constants';

const getCollection = (context) => {
  context.commit('DATA_LOADED', []);

  api.fetchClients().then(
    response => context.commit('DATA_LOADED', response),
    error => context.commit('DATA_LOADED_ERROR', error),
  );
};

const getSchema = (context) => {
  context.commit('DATA_SCHEMA', {});

  api.fetchSchema().then(
    response => context.commit('DATA_SCHEMA', response),
    // error => context.commit('DATA_SCHEMA_ERROR', error),
    error => context.commit('API_ERROR', error),
  );
};

const changePaginationPage = (context, newPageNumber) => {
  context.commit('PAGINATION_CHANGE', newPageNumber);
};

const changeItemsPerPage = (context, itemsPerPage) => {
  context.commit('ITEMS_PER_PAGE', itemsPerPage);
};

const startEdit = (context, data) => {
  context.commit('DATA_EDIT_STATE', data);
};
const stopEdit = context => context.commit('DATA_EDIT_STATE', {});
const startCreate = context => context.commit('DATA_CREATE_STATE', true);
const stopCreate = context => context.commit('DATA_CREATE_STATE', false);

// dispatcher => `${STORE_KEY}/create`
const create = (context, data) => {
  api.createClient(data).then((response) => {
    const { error } = response;

    if (error) {
      context.commit('API_ERROR', error);
    } else {
      context.commit('DATA_CREATED', response);
      stopCreate(context);
    }
  });
};

const update = (context, data) => {
  let isUpdateNecessary = false;

  context.rootState[STORE_KEY].collection.forEach((item) => {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
    if (item._id === data._id) {
      if (JSON.stringify(data) !== JSON.stringify(item)) {
        isUpdateNecessary = true;
      }
    }
  });

  if (isUpdateNecessary) {
    api.updateClient(data).then((response) => {
      const { error } = response;

      if (error) {
        context.commit('DATA_UPDATED_ERROR', error);
      } else {
        context.commit('DATA_UPDATED', response);
        stopEdit(context);
      }
    });
  }
};
const edit = context => context.commit('DATA_EDIT', {});
const deleteit = context => context.commit('DATA_DELETE', {});

export default {
  startEdit,
  stopEdit,
  startCreate,
  stopCreate,
  getCollection,
  getSchema,
  create,
  update,
  edit,
  deleteit,
  changePaginationPage,
  changeItemsPerPage,
};
