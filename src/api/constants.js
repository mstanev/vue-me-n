// TODO: make it reading from .env
const API_BASE_URL = 'http://10.0.1.4:4040';
const other = null;

const GET = () => { 'GET'; };

const POST = (data) => {
  const headers = {
    method: 'POST',
    body: JSON.stringify(data),
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'content-type': 'application/json',
    },
    mode: 'cors',
  };

  return headers;
};

const PUT = (data) => {
  const headers = {
    method: 'PUT',
    body: JSON.stringify(data),
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'content-type': 'application/json',
    },
    mode: 'cors',
  };

  return headers;
};

export { API_BASE_URL, POST, PUT, GET, other };
