/**
 * Map Form Schema to View Model
 *  - designed for crossplatform
 * @param schema
 * @param data
 * @returns {{}}
 */
export const mapSchemaToModel = (schema, data) => {
  const newModel = { ...data };
  const schemaKeys = Object.keys(schema);

  if (!schemaKeys.length) {
    throw Error('Reason: initializeDefaultProps === true | schema is required');
  }

  schemaKeys.forEach((key) => {
    const defaultVal = schema[key].default;

    if (!newModel[key]) {
      newModel[key] = (defaultVal !== undefined) ? defaultVal : '';
    }
  });

  return newModel;
};
