/**
 * {name-space} Vuex Base Module
 * TODO: Description ...
 */

function isStoreRegister() {
  // eslint-disable-next-line no-underscore-dangle
  return this.STORE_KEY in this.$store._modules.root._children;
}

/**
 * Provide Basic functionality about module/container
 * -------------------------
 * Depends on STORE_KEY
 * @example
 data() {
      return {
        store,
        STORE_KEY: '$_storeName',
      };
    },
 * @type {{created(): void, destroyed(): void}}
 * @typedef {BaseModule}
 */
export const BaseModule = {
  created() {
    if (!this.STORE_KEY) {
      throw new Error(this, 'Usage of BaseModule require unique STORE_KEY in the child data');
    }

    if (!isStoreRegister.call(this)) {
      this.$store.subscribeAction(() => {
        // window.localStorage[this.STORE_KEY] = JSON.stringify(this.store.state);
      });

      if (!isStoreRegister.call(this)) {
        this.$store.registerModule(
          this.STORE_KEY,
          this.store,
        );
      }
    }
  },
  destroyed() {
    if (isStoreRegister.call(this)) {
      this.$store.unregisterModule(this.STORE_KEY);
    }
  },
};
