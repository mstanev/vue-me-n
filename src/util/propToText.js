/**
 * {name-space} Prop to Text
 * @description Basically about transition JSON proprties to strings/names
 * @example
 *  propToText('mobileNumber') => 'Mobile Number'
 *  propToText('SomeBooleanProp') => 'Some Boolean Prop'
 *  propToText('Test_StrangeString_asd') => 'Test Strange String Asd'
 */

import titleize from 'titleize';
import humanizeString from 'humanize-string';

/**
 * "titleize" + "humanizeString"
 * @param {string} text
 * @return {string}
 */
export const propToText = text => titleize(humanizeString(text));
